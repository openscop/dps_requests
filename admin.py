from __future__ import unicode_literals

from django.contrib import admin
from django.contrib.contenttypes.admin import GenericTabularInline

from .models import DPSRequest, MedicalContact
from .forms import MedicalContactAdminForm


class MedicalContactInline(GenericTabularInline):
    model = MedicalContact
    form = MedicalContactAdminForm
    extra = 0


admin.site.register(DPSRequest)
