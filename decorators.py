from __future__ import unicode_literals

from django.contrib.auth import REDIRECT_FIELD_NAME
from django.shortcuts import render

from allauth.account.decorators import verified_email_required

from administration.models import Observer, Rescuer

from .models import DPSRequest


def rescuer_required(function=None,
                     login_url=None,
                     redirect_field_name=REDIRECT_FIELD_NAME):
    """
    Restricts page access to rescuers only
    """
    def decorator(view_func):
        @verified_email_required()
        def _wrapped_view(request, *args, **kwargs):
            try:
                request.user.observer.rescuer
            except Observer.DoesNotExist:
                return render(request, 'events/access_restricted.html')
            except Rescuer.DoesNotExist:
                return render(request, 'events/access_restricted.html')
            return view_func(request, *args, **kwargs)
        return _wrapped_view

    if function:
        return decorator(function)
    return decorator


def rescuer_owner_required(function=None,
                           login_url=None,
                           redirect_field_name=REDIRECT_FIELD_NAME):
    """
    Restricts page access to rescuers owner only
    """
    def decorator(view_func):
        @rescuer_required()
        def _wrapped_view(request, *args, **kwargs):
            if request.user.observer.rescuer.association == DPSRequest.objects.get(pk=kwargs['pk']).first_aid_association:  # noqa
                return view_func(request, *args, **kwargs)
            else:
                return render(request, 'events/access_restricted.html')
        return _wrapped_view

    if function:
        return decorator(function)
    return decorator
