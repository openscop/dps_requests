from __future__ import unicode_literals

import factory

from administration.factories import FireServiceFactory

from emergencies.factories import FirstAidAssociationFactory
from emergencies.factories import PublicAssistanceFactory

from .models import DPSRequest
from .models import Intervention, Evacuation, MedicalContact


class DPSRequestFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = DPSRequest

    first_aid_association = factory.SubFactory(FirstAidAssociationFactory)
    rescues_day_shift = True
    rescues_night_shift = True
    area_length = 1000
    age_scale_from = 7
    age_scale_to = 77
    people_with_special_needs = 'No'
    public_attendance_duration = 54
    public_declared = 1000
    gathering_activity = 'l'
    site_accessibility = 'l'
    intervention_time = '0'
    ris = 2
    permanent_structure = True
    non_permanent_structure = True
    public_space = True
    stretcher_range = 1200
    ground_slope = 15
    other_access_difficulties = 'No'
    fire_service = factory.SubFactory(FireServiceFactory)
    nearest_hospital = 'EastBamboFuck Hospital'
    public_assistance = factory.SubFactory(PublicAssistanceFactory)
    logistic_available = 'No'
    meal_provided = True
    kitchen_available = True
    participants_attendance_duration = 10  # pylint: disable=C0103


class InterventionFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = Intervention

    sort = 'type of intervention'
    number = 3
    dps_request = factory.SubFactory(DPSRequestFactory)


class EvacuationFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = Evacuation

    sort = 'type of evacuation'
    number = 3
    dps_request = factory.SubFactory(DPSRequestFactory)


class MedicalContactFactory(factory.django.DjangoModelFactory):
    FACTORY_FOR = MedicalContact

    first_name = 'john'
    last_name = 'doe'
    phone = '06 15 15 15 15'
    activity = 'd'
