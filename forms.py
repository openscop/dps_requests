from __future__ import unicode_literals

from django.utils.translation import ugettext_lazy as _

from ddcs_loire.utils import GenericForm

from crispy_forms.layout import Layout, Fieldset, Div
from crispy_forms.bootstrap import AppendedText

from extra_views import InlineFormSet
from extra_views.generic import GenericInlineFormSet

from localflavor.fr.forms import FRPhoneNumberField

from contacts.forms import ContactAdminForm

from .models import DPSRequest
from .models import Intervention, Evacuation, MedicalContact


class MedicalContactAdminForm(ContactAdminForm):

    class Meta:  # pylint: disable=C1001
        model = MedicalContact
        exclude = ('content_type', 'object_id', 'content_object')


class InterventionForm(GenericForm):

    class Meta:  # pylint: disable=C1001
        model = Intervention
        fields = ('sort', 'number',)

    def __init__(self, *args, **kwargs):  # pylint: disable=E1002
        super(InterventionForm, self).__init__(*args, **kwargs)
        self.helper.form_tag = False


class EvacuationForm(GenericForm):

    class Meta:  # pylint: disable=C1001
        model = Evacuation
        fields = ('sort', 'number',)

    def __init__(self, *args, **kwargs):  # pylint: disable=E1002
        super(EvacuationForm, self).__init__(*args, **kwargs)
        self.helper.form_tag = False


class MedicalContactForm(GenericForm):
    phone = FRPhoneNumberField()

    class Meta:  # pylint: disable=C1001
        model = MedicalContact
        exclude = ('content_type', 'object_id', 'content_object')

    def __init__(self, *args, **kwargs):  # pylint: disable=E1002
        super(MedicalContactForm, self).__init__(*args, **kwargs)
        self.fields['phone'] = FRPhoneNumberField()
        self.helper.form_tag = False


class InterventionInline(InlineFormSet):
    model = Intervention
    fields = ('sort', 'number',)
    extra = 2
    form_class = InterventionForm


class EvacuationInline(InlineFormSet):
    model = Evacuation
    fields = ('sort', 'number',)
    extra = 2
    form_class = EvacuationForm


class MedicalContactInline(GenericInlineFormSet):
    model = MedicalContact
    fields = ('activity', 'first_name', 'last_name', 'phone',)
    extra = 2
    form_class = MedicalContactForm


class DPSRequestForm(GenericForm):

    class Meta:  # pylint: disable=C1001
        model = DPSRequest
        exclude = ('content_type', 'object_id', 'content_object', 'ris')

    def __init__(self, *args, **kwargs):  # pylint: disable=E1002
        super(DPSRequestForm,
              self).__init__(*args, **kwargs)
        self.helper.form_tag = False
        self.helper.layout = Layout(
            Fieldset(
                _('DPS request').capitalize(),
                'first_aid_association',
            ),
            Fieldset(
                _('rescues shifts').capitalize(),
                'rescues_day_shift',
                'rescues_night_shift',
            ),
            Fieldset(
                _('nature of the DPS request').capitalize(),
                Div('age_scale_from', 'age_scale_to'),
                'people_with_special_needs',
                'specific_risks',
                AppendedText('public_attendance_duration', 'heures'),
                AppendedText('participants_attendance_duration', 'heures'),
                AppendedText('public_declared', 'personnes'),
                'gathering_activity',
                'site_accessibility',
                'intervention_time',
            ),
            Fieldset(
                _("site's accessibility").capitalize(),
                'permanent_structure',
                'non_permanent_structure',
                'public_space',
                AppendedText('area_length', 'km'),
                AppendedText('stretcher_range', 'm'),
                AppendedText('ground_slope', '%'),
                'other_access_difficulties',
            ),
            Fieldset(
                _('nearest emergencies services').capitalize(),
                'fire_service',
                'nearest_hospital',
            ),
            Fieldset(
                _('logistics').capitalize(),
                'logistic_available',
                'meal_provided',
                'kitchen_available',
            ),
            Fieldset(
                _('other emergencies on the spot').capitalize(),
                'public_assistance',
            ),
        )
