��    L      |  e   �      p     q     }     �     �     �     �     �     �  	   �                    ,     =     K  
   X     c     i     v  	   �     �     �     �     �     �     �     �     �          /  	   D     N     b     f     t     �     �     �     �     �     �     �     	     #	     4	     L	     S	     f	     l	     �	      �	     �	     �	     �	     �	     

     (
     :
     L
     g
  
   t
     
     �
     �
     �
     �
     �
     �
               )     9     @     C     H  �  N     �     �  	          C   !     e  =   ~  =   �     �     
          "     9     V     p     �     �      �  '   �     �     �          %     -     4  )   A     k     �     �  !   �  
   �  %   �             &     &   ;     b     t     �     �     �     �  4   �          ,     E     L  	   b  $   l  "   �  $   �  	   �  (   �          !  (   4     ]     o          �     �  %   �  !   �  #   �  '         H  C   ^  :   �  +   �     	     '  	   ?     I     L     Q        E       $   L   2                     K             6              G   *      
   !              0         @   .       7          /   <              &              +      J          "   )       A      5   %             H   8   D   ,       ;      9   ?   #   I   :   (   F          -             C             >   3          1   '          B   	       =             4    DPS request DPS requests activity age scale : from area length associated dps request between 10 and 20 minutes between 20 and 30 minutes dashboard doctor dps request dps request created dps request form dps requested dps requests evacuation event fire service first aid association selected for event gathering activity ground slope high hours intervention intervention time kitchen available last edition evacuations last edition interventions less than 10 minutes logistics logistics available low meal provided medical contact medical contacts medical request medical requests medium more than 30 minutes nature of the DPS request nature of the dps request nearest emergencies services nearest hospital non permanent structure number number of rescuers nurse other access difficulties other emergencies on the spot participants attendance duration people people with special needs permanent structure physiotherapist predictable public attendance private ambulance public assistance public attendance duration public space reasonable rescuers attendance by day rescuers attendance by night rescues shifts ris site accessibility site's accessibility sort help text specific risks specific risks help text stretcher range submit to type years Project-Id-Version: medical_request VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-03-08 15:59+0100
PO-Revision-Date: 2014-03-08 16:00+0100
Last-Translator: Simon Panay <simon.panay@openscop.fr>
Language-Team: FR <LL@li.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 1.5.4
 demande de DPS demandes de DPS activité tranche d'âges : de distance maximale entre les deux points les plus éloignés du site demande de DPS associée supérieur à 10 minutes et inférieur ou égal à 20 minutes supérieur à 20 minutes et inférieur ou égal à 30 minutes tableau de bord médecin demande de DPS demande de DPS créée formulaire de demande de DPS demande de DPS effectuée demandes de DPS évacuation manifestation Centres d'incendie et de secours association de premiers secours choisie pour la manifestation activité du rassemblement pente du terrain élevé heures intervention délai d'intervention des secours publics cuisine mise à disposition évacuations en N-1 interventions N-1 inférieur ou égal à 10 minutes logistique moyens logistiques mis à disposition faible repas fourni coordonnées d'un personnel de secours coordonnées des personnels de secours demande médicale demandes médicales moyen supérieur à 30 minutes nature de la demande de DPS nature de la demande de DPS structures fixes de secours publics les plus proches structure hospitalière structure non permanente nombre nombre de secouristes infirmier autres conditions d'accès difficile autres moyens de secours présents durée de présence des participants personnes personnes ayant des besoins particuliers structure permanente kinésithérapeute effectif prévisible déclaré du public ambulance privée secours publics durée de présence du public voie publique modéré présence des secouristes en journée présence des secouristes de nuit durée de présence des secouristes Ration d'Intervenants Secouristes (RIS) accessiblité du site caractéristiques de l'environnement et de l'accessibilité du site accidentologie (fracture, arrêt cardiaque, insolation...) risques spécifiques liés à l'évènement exemple : buvette en place... distance de brancardage soumettre à type années 