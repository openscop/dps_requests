# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('administration', '0001_initial'),
        ('contacts', '0001_initial'),
        ('emergencies', '0001_initial'),
        ('contenttypes', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='DPSRequest',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('object_id', models.PositiveIntegerField()),
                ('rescues_day_shift', models.BooleanField(default=False, verbose_name='rescuers attendance by day')),
                ('rescues_night_shift', models.BooleanField(default=False, verbose_name='rescuers attendance by night')),
                ('area_length', models.PositiveIntegerField(verbose_name='area length')),
                ('age_scale_from', models.PositiveIntegerField(verbose_name='age scale : from')),
                ('age_scale_to', models.PositiveIntegerField(verbose_name='to')),
                ('people_with_special_needs', models.TextField(verbose_name='people with special needs')),
                ('public_attendance_duration', models.PositiveIntegerField(verbose_name='public attendance duration')),
                ('participants_attendance_duration', models.PositiveIntegerField(verbose_name='participants attendance duration')),
                ('public_declared', models.PositiveIntegerField(verbose_name='predictable public attendance')),
                ('specific_risks', models.TextField(help_text='specific risks help text', verbose_name='specific risks', blank=True)),
                ('gathering_activity', models.CharField(max_length=1, verbose_name='gathering activity', choices=[('l', 'low'), ('r', 'reasonable'), ('m', 'medium'), ('h', 'high')])),
                ('site_accessibility', models.CharField(max_length=1, verbose_name='site accessibility', choices=[('l', 'low'), ('r', 'reasonable'), ('m', 'medium'), ('h', 'high')])),
                ('intervention_time', models.CharField(max_length=1, verbose_name='intervention time', choices=[('0', 'less than 10 minutes'), ('1', 'between 10 and 20 minutes'), ('2', 'between 20 and 30 minutes'), ('3', 'more than 30 minutes')])),
                ('ris', models.DecimalField(verbose_name='ris', max_digits=10, decimal_places=2)),
                ('permanent_structure', models.BooleanField(default=False, verbose_name='permanent structure')),
                ('non_permanent_structure', models.BooleanField(default=False, verbose_name='non permanent structure')),
                ('public_space', models.BooleanField(default=False, verbose_name='public space')),
                ('stretcher_range', models.PositiveIntegerField(verbose_name='stretcher range')),
                ('ground_slope', models.PositiveIntegerField(verbose_name='ground slope')),
                ('other_access_difficulties', models.TextField(verbose_name='other access difficulties')),
                ('nearest_hospital', models.CharField(max_length=255, verbose_name='nearest hospital')),
                ('logistic_available', models.CharField(max_length=255, verbose_name='logistics available')),
                ('meal_provided', models.BooleanField(default=False, verbose_name='meal provided')),
                ('kitchen_available', models.BooleanField(default=False, verbose_name='kitchen available')),
                ('content_type', models.ForeignKey(to='contenttypes.ContentType')),
                ('fire_service', models.ForeignKey(verbose_name='fire service', to='administration.FireService')),
                ('first_aid_association', models.ForeignKey(verbose_name='first aid association selected', to='emergencies.FirstAidAssociation')),
                ('public_assistance', models.ForeignKey(verbose_name='public assistance', to='emergencies.PublicAssistance')),
            ],
            options={
                'abstract': False,
                'verbose_name': 'DPS request',
                'verbose_name_plural': 'DPS requests',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Evacuation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('sort', models.CharField(help_text='sort help text', max_length=255, verbose_name='type')),
                ('number', models.PositiveIntegerField(verbose_name='number')),
                ('dps_request', models.ForeignKey(verbose_name='dps request', to='medical_requests.DPSRequest')),
            ],
            options={
                'verbose_name': 'evacuation',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Intervention',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('sort', models.CharField(help_text='sort help text', max_length=255, verbose_name='type')),
                ('number', models.PositiveIntegerField(verbose_name='number')),
                ('dps_request', models.ForeignKey(verbose_name='dps request', to='medical_requests.DPSRequest')),
            ],
            options={
                'verbose_name': 'intervention',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='MedicalContact',
            fields=[
                ('contact_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='contacts.Contact')),
                ('activity', models.CharField(max_length=1, verbose_name='activity', choices=[('d', 'doctor'), ('n', 'nurse'), ('p', 'physiotherapist'), ('a', 'private ambulance')])),
            ],
            options={
                'verbose_name': 'medical contact',
                'verbose_name_plural': 'medical contacts',
            },
            bases=('contacts.contact',),
        ),
        migrations.AlterUniqueTogether(
            name='dpsrequest',
            unique_together=set([('content_type', 'object_id')]),
        ),
    ]
