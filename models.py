from __future__ import unicode_literals

from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.fields import GenericRelation
from django.contrib.contenttypes.models import ContentType
from django.core.urlresolvers import reverse
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.encoding import python_2_unicode_compatible, smart_text
from django.utils.translation import ugettext_lazy as _

from administration.models import FireService

from emergencies.models import FirstAidAssociation
from emergencies.models import PublicAssistance

from contacts.models import Contact

from ddcs_loire.utils import add_notification_entry


class MedicalRequest(models.Model):
    first_aid_association = models.ForeignKey(
        FirstAidAssociation,
        verbose_name=_('first aid association selected'),
    )
    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')

    class Meta:  # pylint: disable=C1001
        abstract = True
        unique_together = ('content_type', 'object_id')
        verbose_name = _('medical request')
        verbose_name_plural = _('medical requests')

    def get_absolute_url(self):
        url = ''.join(['events:',
                       ContentType.objects.get(pk=self.content_type.id).model,
                       '_detail'])
        return reverse(url, kwargs={'pk': self.object_id})


@python_2_unicode_compatible
class MedicalContact(Contact):
    ACTIVITY_CHOICES = (
        ('d', _('doctor')),
        ('n', _('nurse')),
        ('p', _('physiotherapist')),
        ('a', _('private ambulance')),
    )

    activity = models.CharField(_('activity'),
                                max_length=1,
                                choices=ACTIVITY_CHOICES)

    class Meta:  # pylint: disable=C1001
        verbose_name = _('medical contact')
        verbose_name_plural = _('medical contacts')

    def __str__(self):
        return ' '.join([self.get_activity_display().capitalize(), '-',
                         self.first_name.capitalize(),
                         self.last_name.capitalize()])


@python_2_unicode_compatible
class DPSRequest(MedicalRequest):
    GATHERING_ACTIVITY_CHOICES = (
        ('l', _('low')),
        ('r', _('reasonable')),
        ('m', _('medium')),
        ('h', _('high')),
    )
    SITE_ACCESSIBILITY_CHOICES = (
        ('l', _('low')),
        ('r', _('reasonable')),
        ('m', _('medium')),
        ('h', _('high')),
    )
    INTERVENTION_TIME_CHOICES = (
        ('0', _('less than 10 minutes')),
        ('1', _('between 10 and 20 minutes')),
        ('2', _('between 20 and 30 minutes')),
        ('3', _('more than 30 minutes')),
    )

    rescues_day_shift = models.BooleanField(_('rescuers attendance by day'),
                                            default=False)
    rescues_night_shift = models.BooleanField(
        _('rescuers attendance by night'),
        default=False,
    )
    area_length = models.PositiveIntegerField(_('area length'))
    age_scale_from = models.PositiveIntegerField(_('age scale : from'))
    age_scale_to = models.PositiveIntegerField(_('to'))
    people_with_special_needs = models.TextField(
        _('people with special needs'),
    )
    public_attendance_duration = models.PositiveIntegerField(
        _('public attendance duration'),
    )
    # pylint: disable=C0103
    participants_attendance_duration = models.PositiveIntegerField(
        _('participants attendance duration'),
    )
    # pylint: enable=C0103
    public_declared = models.PositiveIntegerField(
        _('predictable public attendance')
    )
    specific_risks = models.TextField(
        _('specific risks'),
        blank=True,
        help_text=_('specific risks help text'),
    )
    gathering_activity = models.CharField(_('gathering activity'),
                                          max_length=1,
                                          choices=GATHERING_ACTIVITY_CHOICES)
    site_accessibility = models.CharField(_('site accessibility'),
                                          max_length=1,
                                          choices=SITE_ACCESSIBILITY_CHOICES)
    intervention_time = models.CharField(_('intervention time'),
                                         max_length=1,
                                         choices=INTERVENTION_TIME_CHOICES)
    ris = models.DecimalField(_('ris'), max_digits=10, decimal_places=2)
    permanent_structure = models.BooleanField(_('permanent structure'),
                                              default=False)
    non_permanent_structure = models.BooleanField(
        _('non permanent structure'),
        default=False,
    )
    public_space = models.BooleanField(_('public space'), default=False)
    stretcher_range = models.PositiveIntegerField(_('stretcher range'))
    ground_slope = models.PositiveIntegerField(_('ground slope'))
    other_access_difficulties = models.TextField(
        _('other access difficulties'),
    )
    fire_service = models.ForeignKey(FireService,
                                     verbose_name=_('fire service'))
    nearest_hospital = models.CharField(_('nearest hospital'), max_length=255)
    public_assistance = models.ForeignKey(PublicAssistance,
                                          verbose_name=_('public assistance'))
    logistic_available = models.CharField(_('logistics available'),
                                          max_length=255)
    meal_provided = models.BooleanField(_('meal provided'), default=False)
    kitchen_available = models.BooleanField(_('kitchen available'),
                                            default=False)
    medical_contact = GenericRelation(
        MedicalContact,
        verbose_name=_('medical contact')
    )

    class Meta(MedicalRequest.Meta):
        verbose_name = _('DPS request')
        verbose_name_plural = _('DPS requests')

    def __str__(self):
        return smart_text(self.content_object.__str__())

    def save(self, *args, **kwargs):  # pylint: disable=E1002
        self.set_ris()
        super(DPSRequest, self).save(*args, **kwargs)

    def set_ris(self):
        # compute public ponderation
        if self.public_declared > 100000:
            public = 100000 + ((self.public_declared - 100000)/2)
        else:
            public = self.public_declared

        # compute intervention time
        intervention_time_ratio = {'0': 0.25, '1': 0.30, '2': 0.35, '3': 0.40}
        intervention_time = intervention_time_ratio[self.intervention_time]

        # compute gathering activity
        gathering_activity_ratio = {'l': 0.25, 'r': 0.30, 'm': 0.35, 'h': 0.40}
        gathering_activity = gathering_activity_ratio[self.gathering_activity]

        # compute intervention time
        site_accessibility_ratio = {'l': 0.25, 'r': 0.30, 'm': 0.35, 'h': 0.40}
        site_accessibility = site_accessibility_ratio[self.site_accessibility]

        # compute risk index
        risk_index = (intervention_time + gathering_activity +
                      site_accessibility)

        # compute ris
        self.ris = round(risk_index * public / 1000, 3)

    def get_rescuers_number(self):
        number = self.ris // 1 + 1
        return number + number % 2

    def notify_creation(self):
        add_notification_entry(
            event=self.content_object,
            agents=self.first_aid_association.rescuer_set.all(),  # noqa pylint: disable=E1101
            subject=_('dps requested'),
            content_object=self.content_object.structure,
        )


@receiver(post_save, sender=DPSRequest)
def log_dps_request_creation(created, instance, **kwargs):
    if created:
        instance.content_object.action_set.create(
            user=instance.content_object.structure.promoter.user,
            action=_('dps request created')
        )
        instance.notify_creation()


@python_2_unicode_compatible
class Intervention(models.Model):
    sort = models.CharField(_('type'), max_length=255,
                            help_text=_('sort help text'))
    number = models.PositiveIntegerField(_('number'))
    dps_request = models.ForeignKey(DPSRequest, verbose_name=_('dps request'))

    class Meta:  # pylint: disable=C1001
        verbose_name = _('intervention')

    def __str__(self):
        return ' '.join(['Intervention -', self.dps_request.__str__()])


@python_2_unicode_compatible
class Evacuation(models.Model):
    sort = models.CharField(_('type'), max_length=255,
                            help_text=_('sort help text'))
    number = models.PositiveIntegerField(_('number'))
    dps_request = models.ForeignKey(DPSRequest, verbose_name=_('dps request'))

    class Meta:  # pylint: disable=C1001
        verbose_name = _('evacuation')

    def __str__(self):
        return ' '.join(['Evacuation -', self.dps_request.__str__()])
