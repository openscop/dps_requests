from __future__ import unicode_literals

from django.contrib.contenttypes.models import ContentType
from django.test import TestCase

from events.factories import DeclaredNonMotorizedEventFactory

from notifications.models import Action

from .factories import DPSRequestFactory
from .factories import InterventionFactory, EvacuationFactory
from .factories import MedicalContactFactory


class DPSRequestMethodTests(TestCase):  # pylint: disable=R0904

    def test_str_(self):
        '''
        __str__() should return the name of the request
        '''
        dps_request = DPSRequestFactory.build()
        self.assertEqual(dps_request.__str__(), 'None')

    def test_set_ris(self):
        dps_request = DPSRequestFactory.build(public_declared=5000,
                                              gathering_activity='r',
                                              site_accessibility='h',
                                              intervention_time='0')
        dps_request.set_ris()
        self.assertEqual(dps_request.ris, 4.75)
        self.assertEqual(dps_request.get_rescuers_number(), 6)

    def test_set_ris2(self):
        dps_request = DPSRequestFactory.build(public_declared=150000,
                                              gathering_activity='r',
                                              site_accessibility='h',
                                              intervention_time='0')
        dps_request.set_ris()
        self.assertEqual(dps_request.ris, 118.75)
        self.assertEqual(dps_request.get_rescuers_number(), 120)

    def test_log_dps_request_creation(self):
        event = DeclaredNonMotorizedEventFactory.create()
        dps_request = DPSRequestFactory.create(
            object_id=event.id,
            content_type=ContentType.objects.get_for_model(event),
        )
        action = Action.objects.last()
        self.assertEqual(action.user, event.structure.promoter.user)
        self.assertEqual(action.event, event.event_ptr)
        self.assertEqual(action.action, "dps request created")
        # when dps request is only updated,
        # no log action is created
        dps_request.save()
        action2 = Action.objects.last()
        self.assertEqual(action, action2)


class InterventionMethodTests(TestCase):  # pylint: disable=R0904

    def test_str_(self):
        '''
        __str__() should return the name of the intervention
        '''
        intervention = InterventionFactory.build()
        self.assertEqual(intervention.__str__(), 'Intervention - None')


class EvacuationMethodTests(TestCase):  # pylint: disable=R0904

    def test_str_(self):
        '''
        __str__() should return the name of the evacuation
        '''
        evacuation = EvacuationFactory.build()
        self.assertEqual(evacuation.__str__(), 'Evacuation - None')


class MedicalContactMethodTests(TestCase):  # pylint: disable=R0904

    def test_str_(self):
        '''
        __str__() should return the activity and the names of
        the contact
        '''
        medical_contact = MedicalContactFactory.build()
        self.assertEqual(medical_contact.__str__(),
                         'Doctor - John Doe')
