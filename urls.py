from __future__ import unicode_literals

from django.conf.urls import patterns, url

from .views import Dashboard
from .views import DPSRequestDetail
from .views import DPSRequestCreate
# from .views import DPSRequestUpdate
from .views import DPSRequestDecline

urlpatterns = patterns(
    # pylint: disable=E1120
    '',
    url(r'^dashboard/$', Dashboard.as_view(),
        name="dashboard"),
    url(r'^dpsrequest/(?P<pk>\d+)/$',
        DPSRequestDetail.as_view(),
        name="dpsrequest_detail"),
    url(r'^dpsrequest/add/(?P<event_pk>\d+)/(?P<ct_pk>\d+)/$',
        DPSRequestCreate.as_view(),
        name='dpsrequest_add'),
    # url(r'^dpsrequest/(?P<pk>\d+)/edit$',
    #     DPSRequestUpdate.as_view(),
    #     name='dpsrequest_update'),
    url(r'^dpsrequest/decline/(?P<pk>\d+)/(?P<ct_pk>\d+)/$',
        DPSRequestDecline.as_view(),
        name='dpsrequest_decline'),
    # pylint: enable=E1120
)
