from __future__ import unicode_literals

from django.contrib.contenttypes.models import ContentType
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.utils.decorators import method_decorator
from django.views.generic import DetailView, ListView
from django.views.generic.base import RedirectView

from extra_views import CreateWithInlinesView
# from extra_views import UpdateWithInlinesView

from events.decorators import promoter_required

from .decorators import rescuer_required, rescuer_owner_required

from .forms import DPSRequestForm
from .forms import InterventionInline, EvacuationInline, MedicalContactInline

from .models import DPSRequest


class Dashboard(ListView):  # pylint: disable=R0901
    model = DPSRequest

    @method_decorator(rescuer_required)
    def dispatch(self, *args, **kwargs):
        return super(Dashboard, self).dispatch(*args, **kwargs)

    def get_queryset(self):
        return DPSRequest.objects.filter(
            first_aid_association=self.request.user.observer.rescuer.association)  # noqa


class DPSRequestDetail(DetailView):  # pylint: disable=R0901
    model = DPSRequest

    @method_decorator(rescuer_owner_required)
    def dispatch(self, *args, **kwargs):
        return super(DPSRequestDetail, self).dispatch(*args, **kwargs)


class DPSRequestCreate(CreateWithInlinesView):  # pylint: disable=R0901
    model = DPSRequest
    form_class = DPSRequestForm
    inlines = [MedicalContactInline, InterventionInline, EvacuationInline]

    @method_decorator(promoter_required)
    def dispatch(self, *args, **kwargs):
        return super(DPSRequestCreate, self).dispatch(*args, **kwargs)

    def forms_valid(self, form, inlines, **kwargs):
        # pylint: disable=E1101
        self.object.content_type = get_object_or_404(
            ContentType,
            pk=self.kwargs['ct_pk'],
        )
        self.object.object_id = self.kwargs['event_pk']
        # pylint: enable=E1101
        self.object = form.save()
        for formset in inlines:
            formset.save()
        return HttpResponseRedirect(self.get_success_url())


class DPSRequestDecline(RedirectView):

    @method_decorator(promoter_required)
    def dispatch(self, *args, **kwargs):
        return super(DPSRequestDecline, self).dispatch(*args, **kwargs)

    def get_redirect_url(self, *args, **kwargs):
        content_type = ContentType.objects.get_for_id(kwargs['ct_pk'])
        event = content_type.get_object_for_this_type(pk=kwargs['pk'])
        event.dps_needed = False
        event.save(update_fields=['dps_needed'])
        url = ''.join(['events:', content_type.model, '_detail'])
        return reverse(url, kwargs={'pk': kwargs['pk']})
#
#
# class DPSRequestUpdate(UpdateWithInlinesView):  # pylint: disable=R0901
#     model = DPSRequest
#     form_class = DPSRequestForm
#     inlines = [MedicalContactInline, InterventionInline, EvacuationInline]
#
#     @method_decorator(promoter_required)
#     def dispatch(self, *args, **kwargs):
#         return super(DPSRequestUpdate, self).dispatch(*args, **kwargs)
#
#     def get_success_url(self):
#         return self.object.get_absolute_url()
